//! <https://projecteuler.net/problem=015>
//!
//! Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
//!
//! ![](https://projecteuler.net/project/images/p015.png)
//!
//! How many such routes are there through a 20×20 grid?
//!

const INPUT: usize = 20;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 2;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 6;

use rug::Integer;

pub fn factorial(num: usize) -> Integer {
    (1..=num).fold(Integer::from(1), |a, v| a * Integer::from(v))
}

pub fn run(input: usize) -> Integer {
    let n = input;
    let part = factorial(n);
    (factorial(n * 2) / &part) / &part
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(1), 2);
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
