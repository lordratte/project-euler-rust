//! <https://projecteuler.net/problem=009>
//!
//! A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
//! a^2 + b^2 = c^2
//!
//! For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
//!
//! There exists exactly one Pythagorean triplet for which a + b + c = 1000.
//! Find the product abc.

const INPUT: isize = 1000;
#[allow(dead_code)]
const EXAMPLE_INPUT: isize = 12;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: isize = 60;

pub fn run(input: isize) -> isize {
    let factors: Vec<_> = (1..input).collect();
    let max = input / 2;
    for c in factors.iter().take_while(|&v| v <= &max) {
        for b in factors.iter().take_while(|&v| v < c) {
            let a = input - (c + b);
            println!("a {} b {} c {}", a, b, c);
            if c.pow(2) == a.pow(2) + b.pow(2) {
                return a * b * c;
            }
        }
    }
    panic!("No triple");
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
