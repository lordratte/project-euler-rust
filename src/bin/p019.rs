//! <https://projecteuler.net/problem=019>
//!
//! You are given the following information, but you may prefer to do some research for yourself.
//!
//! * 1 Jan 1900 was a Monday.
//!
//! * Thirty days has September,
//! April, June and November.
//! All the rest have thirty-one,
//! Saving February alone,
//! Which has twenty-eight, rain or shine.
//! And on leap years, twenty-nine.
//!
//! * A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
//!
//! How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

const INPUT: usize = 2000;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 1901;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 2;

const START_YEAR: usize = 1901;
const START_DAY: usize = 2;

/// Find the number of Sundays that fall on the 1st of a month between January 1 of START_YEAR and
/// December 31 of `input`
pub fn run(input: usize) -> usize {
    let years = input;
    let mut count = 0;
    let mut day = START_DAY;
    for year in START_YEAR..=years {
        for m in 1..=12 {
            day =
                (day + match m {
                    // 31 % 7
                    1 | 3 | 5 | 7 | 8 | 10 | 12 => 3,
                    // 30 % 7
                    4 | 6 | 9 | 11 => 2,
                    // (28 or 29) % 7
                    2 => {
                        if year % 4 == 0 && (year % 100 != 0 || year % 400 == 0) {
                            1
                        } else {
                            0
                        }
                    }
                    _ => panic!("Invalid month {}", m),
                }) % 7;
            if day == 0 {
                count += 1;
            }
        }
    }

    count
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
