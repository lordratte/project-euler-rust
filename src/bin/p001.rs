//! <https://projecteuler.net/problem=1>
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
//!
//! Find the sum of all the multiples of 3 or 5 below 1000.
//!

const INPUT: isize = 1000;
#[allow(dead_code)]
const EXAMPLE_INPUT: isize = 10;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: isize = 23;

pub fn run(input: isize) -> isize {
    let mut tot: isize = 0;
    for i in 1..input {
        match (i % 3) * (i % 5) {
            0 => tot += i,
            _ => (),
        };
    }
    tot
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
