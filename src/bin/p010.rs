//! <https://projecteuler.net/problem=010>
//!
//! The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
//!
//! Find the sum of all the primes below two million.

const INPUT: usize = 2000000;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 10;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 17;

pub fn run(input: usize) -> usize {
    let mut primes: Vec<usize> = Vec::new();
    if input >= 2 {
        primes.push(2);
    }

    println!("");
    for n in (3..input).step_by(2) {
        print!("\r{:?}%", n * 100 / input);
        if !primes
            .iter()
            .take_while(|v| v.pow(2) <= n)
            .any(|&v| n % v == 0)
        {
            primes.push(n);
        }
    }
    println!("");

    primes.iter().sum()
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
