//! <https://projecteuler.net/problem=012>
//!
//! The sequence of triangle numbers is generated by adding the natural numbers. So the 7th triangle number would be 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. The first ten terms would be:
//!
//! 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
//!
//! Let us list the factors of the first seven triangle numbers:
//!
//!      1: 1
//!      3: 1,3
//!      6: 1,2,3,6
//!     10: 1,2,5,10
//!     15: 1,3,5,15
//!     21: 1,3,7,21
//!     28: 1,2,4,7,14,28
//!
//! We can see that 28 is the first triangle number to have over five divisors.
//!
//! What is the value of the first triangle number to have over five hundred divisors?
//!
//!

const INPUT: isize = 500;
#[allow(dead_code)]
const EXAMPLE_INPUT: isize = 5;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: isize = 28;

pub fn run(input: isize) -> isize {
    let mut triag = 0_isize;
    for inc in 1.. {
        triag += inc;
        let mut divisors = 0;
        let mut t = 1_isize;
        println!("--{}", triag);
        while t.pow(2) < triag {
            if triag % t == 0 {
                divisors += 1;
            }
            if 2 * divisors > input {
                return triag;
            }
            t += 1;
        }
    }
    panic!();
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
