//! <https://projecteuler.net/problem=014>
//!
//! The following iterative sequence is defined for the set of positive integers:
//!
//! n → n/2 (n is even)
//! n → 3n + 1 (n is odd)
//!
//! Using the rule above and starting with 13, we generate the following sequence:
//! 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
//!
//! It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
//!
//! Which starting number, under one million, produces the longest chain?
//!
//! NOTE: Once the chain starts the terms are allowed to go above one million.
//!
//!

const INPUT: usize = 1000000;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 14;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 9;

pub fn run(input: usize) -> usize {
    let mut max_ind = 0;
    let mut max_val = 0;
    for i in 1..input {
        let mut n = i;
        let mut count = 0;
        while n != 1 {
            count += 1;
            n = if n % 2 == 0 { n / 2 } else { n * 3 + 1 }
        }
        if max_val < count {
            max_val = count;
            max_ind = i;
        }
    }
    max_ind
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
