#![feature(const_eval_limit)]
#![const_eval_limit = "18446744073709551615"]
//! <https://projecteuler.net/problem=022>
//!
//! Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.
//!
//! For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.
//!
//! What is the total of all the name scores in the file?
//!

const INPUT: usize = 0;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 0;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 0;
use const_sort::{sorted_indexes, str_lt};

const NUM_ITEMS: usize = include!("../../data/p022_names.txt").len();

const ANSWER: usize = {
    let data = include!("../../data/p022_names.txt");
    parse_data(data)
};

const fn sorted(array: [&str; NUM_ITEMS]) -> [&str; NUM_ITEMS] {
    let mut new = [""; NUM_ITEMS];
    let indexes = sorted_indexes!(&str, str_lt)(&array);
    let mut i = 0;
    while i < NUM_ITEMS {
        new[i] = array[indexes[i]];
        i += 1;
    }
    new
}

const fn parse_data(names: [&str; NUM_ITEMS]) -> usize {
    let names = sorted(names);
    let mut i = 0;
    let mut tot = 0;
    while i < NUM_ITEMS {
        tot += alphavalue(names[i]) * (i + 1);
        i += 1;
    }
    tot
}

const fn alphavalue(word: &str) -> usize {
    let mut tot = 0;
    let mut i = 0;
    let word = word.as_bytes();
    while i < word.len() {
        tot += (word[i] - 64) as usize;
        i += 1;
    }
    tot
}

pub fn run(_input: usize) -> usize {
    ANSWER
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
