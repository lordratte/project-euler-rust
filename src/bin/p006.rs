//! <https://projecteuler.net/problem=006>
//!
//! The sum of the squares of the first ten natural numbers is,
//!
//! 1^2 + 2^2 + ... + 10^2 == 385
//!
//! The square of the sum of the first ten natural numbers is,
//!
//! (1 + 2 + ... + 10)^2 == 55^2 = 3025
//!
//! Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is
//!
//! 3025 - 285 == 2640
//!
//! Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

const INPUT: isize = 100;
#[allow(dead_code)]
const EXAMPLE_INPUT: isize = 10;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: isize = 2640;

pub fn run(input: isize) -> isize {
    let mut tot = ((input.pow(2) + input) / 2).pow(2);
    for i in 1..=input {
        tot -= i.pow(2);
    }
    tot
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
