//! <https://projecteuler.net/problem=007>
//!
//! By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
//!
//! What is the 10 001st prime number?

const INPUT: isize = 10001;
#[allow(dead_code)]
const EXAMPLE_INPUT: isize = 6;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: isize = 13;

fn is_prime(number: isize) -> bool {
    if number < 2 {
        false
    } else if number == 2 || number == 3 {
        true
    } else if number % 2 == 0 {
        false
    } else {
        let s = (number as f32).sqrt() as isize;
        for i in (3..=s).step_by(2) {
            if number % i == 0 {
                return false;
            }
        }
        true
    }
}

pub fn run(input: isize) -> isize {
    let mut i = 1;
    let mut prime = 2;
    while i < input {
        if prime == 2 {
            prime = 1
        };
        for n in (prime..).step_by(2).skip(1) {
            if is_prime(n) {
                prime = n;
                i += 1;
                break;
            }
        }
    }
    prime
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
