//! <https://projecteuler.net/problem=4>
//!
//! A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
//!
//! Find the largest palindrome made from the product of two 3-digit numbers.

const INPUT: isize = 3;
#[allow(dead_code)]
const EXAMPLE_INPUT: isize = 2;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: isize = 9009;

fn is_palindrome(val: isize) -> bool {
    let s = val.to_string();
    s == s.chars().rev().collect::<String>()
}

pub fn run(input: isize) -> isize {
    let mut max = 0;
    for a in (10_isize.pow(input as u32 - 1)..10_isize.pow(input as u32)).rev() {
        for b in (a..10_isize.pow(input as u32)).rev() {
            if is_palindrome(a * b) {
                if max < a * b {
                    max = a * b;
                }
            }
        }
    }
    max
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
