//! <https://projecteuler.net/problem=016>
//!
//! 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
//!
//! What is the sum of the digits of the number 2^1000?
//!

const INPUT: usize = 1000;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 15;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 26;

use rug::{Complete, Integer};

pub fn run(input: usize) -> usize {
    Integer::u_pow_u(2, input.try_into().unwrap())
        .complete()
        .to_string()
        .split("")
        .filter_map::<usize, _>(|d| d.parse().ok())
        .sum::<usize>()
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
