//! <https://projecteuler.net/problem=017>
//!
//! If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
//!
//! If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
//!
//! NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
//!
//!

const INPUT: usize = 1000;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 5;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 19;

fn unit(num: usize) -> usize {
    match num {
        1 | 2 | 6 => 3,
        4 | 5 | 9 => 4,
        3 | 7 | 8 => 5,
        e => panic!("{} is not a valid unit", e),
    }
}

fn ten(num: usize) -> usize {
    match num {
        5 | 6 | 4 => 5,
        2 | 3 | 8 | 9 => 6,
        7 => 7,
        e => panic!("{} is not a valid ten", e),
    }
}

fn hundred(num: usize) -> usize {
    unit(num) + 7 // num + "hundred"
}

fn thousand(num: usize) -> usize {
    unit(num) + 8 // num + "thousand"
}

fn num_len(num: usize) -> usize {
    let mut len = 0;
    let thousands = num / 1000;
    let hundreds = (num / 100) % 10;
    let tens = (num / 10) % 10;
    let units = num % 10;
    if thousands > 0 {
        len += thousand(thousands);
    }
    if hundreds > 0 {
        len += hundred(hundreds);
        if tens + units > 0 {
            len += 3 // "and"
        }
    }
    if tens > 1 {
        len += ten(tens);
    }
    if tens != 1 {
        if units > 0 {
            len += unit(units);
        }
    } else {
        len += match units {
            0 => 3,
            1 | 2 => 6,
            5 | 6 => 7,
            3 | 4 | 8 | 9 => 8,
            7 => 9,
            e => panic!("{} is not a valid number from 10 to 19 (inclusive)", e),
        }
    }
    len
}

pub fn run(input: usize) -> usize {
    let mut len = 0;
    for n in 1..=input {
        len += num_len(n);
    }
    len
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
    assert_eq!(num_len(342), 23);
    assert_eq!(num_len(115), 20);
}
