//! <https://projecteuler.net/problem=021>
//!
//! Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
//! If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.
//!
//! For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
//!
//! Evaluate the sum of all the amicable numbers under 10000.
//!

const INPUT: usize = 10000;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 1185;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 1688;

use std::collections::HashSet;

/// Sum of proper divisors
fn proper_sum(a: usize) -> usize {
    let mut tally = 0;
    let max = (a as f64).sqrt() as usize;
    for d in 1..=max {
        if a % d == 0 {
            tally += d + (a / d);
        }
    }
    tally - a
}

pub fn run(input: usize) -> usize {
    let mut amicable = HashSet::new();
    for x in 1..input {
        if amicable.contains(&x) {
            continue;
        }
        let prsum = proper_sum(x);
        if prsum != x && proper_sum(prsum) == x {
            amicable.insert(x);
            amicable.insert(prsum);
        }
    }
    amicable.iter().filter(|v| **v < input).sum()
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
