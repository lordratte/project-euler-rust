//! <https://projecteuler.net/problem=3>
//!
//! The prime factors of 13195 are 5, 7, 13 and 29.
//!
//! What is the largest prime factor of the number 600851475143 ?

const INPUT: isize = 600851475143;
#[allow(dead_code)]
const EXAMPLE_INPUT: isize = 13195;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: isize = 29;

fn is_prime(number: isize) -> bool {
    if number < 2 {
        false
    } else if number == 2 || number == 3 {
        true
    } else if number % 2 == 0 {
        false
    } else {
        let s = (number as f32).sqrt() as isize;
        for i in (3..=s).step_by(2) {
            if number % i == 0 {
                return false;
            }
        }
        true
    }
}

pub fn run(input: isize) -> isize {
    let mut prime = 2;
    let mut number = input;
    let mut max_prime_fac = prime;

    let mut diff = 4;

    loop {
        // Is this prime a factor?
        if number % prime == 0 {
            println!("{}", prime);
            max_prime_fac = prime;
            number = number / prime;
        }

        // We have all prime factors
        if number == 1 {
            break;
        }

        // Get from 2 to first, odd base case
        if prime == 2 {
            prime = 3;
        }

        diff = 4 - ((diff + 6) % 4);

        loop {
            prime += diff;
            if prime > number {
                return max_prime_fac;
            }
            if is_prime(prime) {
                break;
            }
        }
    }

    prime
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
    assert_eq!(run(100), 5);
}
