//! <https://projecteuler.net/problem=018>
//!
//! By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
//!
//! ```
//!    3
//!   7 4
//!  2 4 6
//! 8 5 9 3
//! ```
//!
//! That is, 3 + 7 + 4 + 9 = 23.
//!
//! Find the maximum total from top to bottom in [triangle.txt](https://projecteuler.net/project/resources/p067_triangle.txt) (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.
//!
//! NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to solve this problem, as there are 299 altogether! If you could check one trillion (1012) routes every second it would take over twenty billion years to check them all. There is an efficient algorithm to solve it. ;o)

mod p018;
use p018::run;
use std::fs;

fn INPUT() -> Vec<Vec<usize>> {
    match fs::read_to_string("data/p067_triangle.txt") {
        Ok(text) => text
            .lines()
            .map(|line| line.split(" ").filter_map(|v| v.parse().ok()).collect())
            .collect(),
        e => panic!("Failed to read file. {:?}", e),
    }
}

#[allow(dead_code)]
fn EXAMPLE_INPUT() -> Vec<Vec<usize>> {
    vec![vec![3], vec![7, 4], vec![2, 4, 6], vec![8, 5, 9, 3]]
}
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 23;

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        panic!("Don't use arguments");
    }
    let input = INPUT();
    println!("{:?}", input);

    println!("The answer for {} is {}", file!(), run(input));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT()), EXAMPLE_OUTPUT);
}
