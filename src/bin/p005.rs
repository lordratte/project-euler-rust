//! <https://projecteuler.net/problem=5>
//!
//! 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//!
//! What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

const INPUT: isize = 20;
#[allow(dead_code)]
const EXAMPLE_INPUT: isize = 10;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: isize = 2520;

pub fn run(input: isize) -> isize {
    let mut lcm = (1..=input).fold(1, |v, a| v * a);

    loop {
        let last = lcm;
        for q in 2..=input {
            let tmp = if lcm % q == 0 {
                lcm / q
            } else {
                continue;
            };
            let mut valid = true;
            for d in 2..=input {
                if tmp % d != 0 {
                    valid = false;
                    break;
                }
            }
            if valid {
                lcm = tmp;
            } else {
                break;
            }
        }
        if last == lcm {
            break;
        }
    }

    lcm
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
