//! <https://projecteuler.net/problem=020>
//!
//!
//!
//! n! means n × (n − 1) × ... × 3 × 2 × 1
//!
//! For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
//! and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
//!
//! Find the sum of the digits in the number 100!
//!
//!

const INPUT: usize = 100;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = 10;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = 27;

use rust_bignum::Big;

pub fn run(input: usize) -> usize {
    let mut num = Big::from(1_u8);
    for i in 1..=input {
        let co: u128 = i.try_into().unwrap();
        num = num * Big::from(co);
    }
    num.digits()
        .iter()
        .map(|x: &u8| {
            let o: usize = (*x).try_into().unwrap();
            o
        })
        .sum()
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
