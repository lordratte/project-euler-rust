//! <https://projecteuler.net/problem=__NAME__>
//!
//! :
//!

const INPUT: usize = __INPUT__;
#[allow(dead_code)]
const EXAMPLE_INPUT: usize = __EXAMPLE_INPUT__;
#[allow(dead_code)]
const EXAMPLE_OUTPUT: usize = __EXAMPLE_OUTPUT__;

pub fn run(input: usize) -> usize {
    input
}

fn main() {
    if let Some(custom) = std::env::args().nth(1) {
        if let Ok(custom) = custom.parse() {
            println!(
                "The answer for {} (with the input, {}) is {}",
                file!(),
                custom,
                run(custom)
            );
            return;
        }
    }

    println!("The answer for {} is {}", file!(), run(INPUT));
}

#[test]
fn example() {
    assert_eq!(run(EXAMPLE_INPUT), EXAMPLE_OUTPUT);
}
