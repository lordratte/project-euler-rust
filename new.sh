#!/bin/bash

function get() {
    if ! IFS= read -r INPUT; then
        exit;
    fi
    printf '%s' "$INPUT"
}

echo -n "Name (001-999): ";
name=$(get)

if [[ $name == "000" || ! $name =~ ^[0-9][0-9][0-9]$ ]]; then
    echo "Invalid name";
    exit;
fi

echo "https://projecteuler.net/problem=$name"

file="src/bin/p${name}.rs"

if [[ -e $file ]]; then
    echo "File exists: $file";
    exit;
fi

echo -n "Input: ";
input=$(get)

echo -n "Example Input: ";
example_input=$(get)

echo -n "Example Output: ";
example_output=$(get)

cp template.rs "$file"

sed -i 's/__NAME__/'"$name"'/g' "$file"; 
sed -i 's/__INPUT__/'"$input"'/g' "$file"; 
sed -i 's/__EXAMPLE_INPUT__/'"$example_input"'/g' "$file"; 
sed -i 's/__EXAMPLE_OUTPUT__/'"$example_output"'/g' "$file"; 
